const gulp = require('gulp'),
  sass = require('gulp-sass'),
  twig = require('gulp-twig'),
  babel = require('gulp-babel'),
  sync = require('browser-sync').create(),
  uglify = require('gulp-uglify'),
  cleanCSS = require('gulp-clean-css'),
  pkg = require('pkg');

sass.compiler = require('node-sass');
const i18next = require('i18next');
var Backend = require('i18next-node-fs-backend');

(async () => {
  // const baseUrl = 'http://localhost:8088';
  const baseUrl = 'https://dtdauto.com';

  const formatNumber = (num, locale) => {
    if (typeof num !== 'number') {
      return `0 ${locale === 'vi' ? ' VND' : ' $'}`;
    }
    const formated = num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
    if (locale === 'vi') {
      return formated + ' VND';
    }
    return formated + '$';
  };

  function getProductByLocale(product, locale) {
    const { variants } = product;
    if (!variants) {
      return {};
    }

    if (!variants.length) {
      return {};
    }

    const price = variants.reduce(
      (current, next) => {
        if (next.priceEn > current.priceMaxEn) {
          current.priceMaxEn = next.priceEn;
        }
        if (next.priceVn > current.priceMaxVn) {
          current.priceMaxVn = next.priceVn;
        }
        if (next.priceEn < current.priceMinEn) {
          current.priceMinEn = next.priceEn;
        }
        if (next.priceVn < current.priceMinVn) {
          current.priceMinVn = next.priceVn;
        }
        return current;
      },
      {
        priceMinEn: variants[0].priceEn,
        priceMaxEn: variants[0].priceEn,
        priceMinVn: variants[0].priceVn,
        priceMaxVn: variants[0].priceVn,
      }
    );
    return locale == 'vi'
      ? {
          id: product.id,
          slug: product.slugVn,
          name: product.nameVn,
          brief: product.briefVn,
          madeBy: product.madeByVn,
          desc: product.descVn,
          options: product.options || [],
          variants: product.variants.map((v) => {
            return {
              price: v.priceVn,
              priceString: formatNumber(v.priceVn, locale),
              sku: v.sku,
              name: v.name,
              weight: v.weight,
              netWeight: v.netWeight,
              variantId: v.id,
              productId: v.productId,
              option1: v.option1,
              option2: v.option2,
              option3: v.option3,
            };
          }),
          priceMin: formatNumber(price.priceMinVn, locale),
          priceMax: formatNumber(price.priceMaxVn, locale),
          images: product.images,
          feature: product.images[0] ? product.images[0].url : '',
        }
      : {
          id: product.id,
          slug: product.slugEn,
          name: product.nameEn,
          brief: product.briefEn,
          madeBy: product.madeByEn,
          desc: product.descEn,
          options: product.options || [],
          variants: product.variants.map((v) => {
            return {
              price: v.priceEn,
              priceString: formatNumber(v.priceEn, locale),
              sku: v.sku,
              name: v.name,
              weight: v.weight,
              netWeight: v.netWeight,
              variantId: v.id,
              productId: v.productId,
              option1: v.option1,
              option2: v.option2,
              option3: v.option3,
            };
          }),
          priceMin: formatNumber(price.priceMinEn, locale),
          priceMax: formatNumber(price.priceMaxEn, locale),
          images: product.images,
          feature: product.images[0] ? product.images[0].url : '',
        };
  }

  const getProductString = (product) => {
    return JSON.stringify(product);
  };

  const banners = [
    {
      id: 1,
      imagePc: {
        id: 1,
        url: 'http://dtdauto.zamio.net/upload/1589970922569-11.jpg',
      },
      imageMobile: {
        id: 2,
        url:
          'https://res.cloudinary.com/css-tricks/image/upload/c_scale,w_1000,f_auto,q_auto/v1531489882/320-version_afwzxa.png',
      },
    },
    {
      id: 1,
      imagePc: {
        id: 1,
        url: 'http://dtdauto.zamio.net/upload/1589970922569-11.jpg',
      },
      imageMobile: {
        id: 2,
        url:
          'https://res.cloudinary.com/css-tricks/image/upload/c_scale,w_1000,f_auto,q_auto/v1531489882/320-version_afwzxa.png',
      },
    },
  ];

  const orderData = {
    currency: '$',
    customerName: '3213213',
    email: 'qw213213',
    phone: '213',
    note: '3123',
    shippingCountry: 20,
    shippingCity: null,
    shippingAddress: '123213',
    shippingCityName: '3213',
    shippingMethod: 'DHL',
    paymentMethod: 'WESTERN',
    shippingFee: 151.5,
    paymentFee: 4.67,
    shippingCountryName: 'Albania',
    createdBy: undefined,
    weight: 2.5,
    netWeight: 2.5,
    total: 238.17,
    id: 25,
    createdDate: '2020-06-30T09:20:49.621Z',
    cartItem: [
      {
        quantity: 1,
        variantId: 5,
        cartId: 25,
        sku: 'EC1006',
        productId: 51,
        weight: 2.5,
        netWeight: 2.5,
        nameVn: 'PCB UNSOLDER - BỘ GHIM MẠCH ĐIỆN KHÔNG CẦN HÀN DÂY',
        nameEn: 'PCB UNSOLDER - ELECTRONIC CIRCUIT HOLDER WITHOUT WELDING',
        productSlugVn: 'PCB-VN1',
        productSlugEn: 'PCB-EN1',
        productImage: 'http://dtdauto.zamio.net/upload/1589949273206-Hinh3.jpg',
        variantTitle: '1100',
        price: 82,
        discount: 0,
        totalPrice: 82,
      },
    ],
  };

  const categories = [
    {
      slugVn: '/danh-muc/thiet-bi-oto',
      slugEn: '/categories/equipment-automobile',
      nameEn: 'Equipment for automobile',
      nameVn: 'Thiết bị ô tô',
      child: [
        {
          slugVn: '/danh-muc/thiet-bi-sua-chua-oto',
          slugEn: '/categories/equipment-for-motobike-repair',
          nameEn: 'Equipment for automobile repair',
          nameVn: 'Thiết bị sửa chữa ô tô',
        },
        {
          slugVn: '/danh-muc/thiet-bi-oto-ca-nhan',
          slugEn: '/categories/equipment-private car',
          nameEn: 'Equipment for private car',
          nameVn: 'Thiết bị mô tô cá nhân',
        },
        {
          slugVn: '/danh-muc/thiet-bi-oto-ca-nhan',
          slugEn: '/categories/equipment-private car',
          nameEn: 'Equipment for automobile training',
          nameVn: 'Thiết bị đào tạo nghề ô tô',
        },
        {
          slugVn: '/danh-muc/thiet-bi-oto-ca-nhan',
          slugEn: '/categories/equipment-private car',
          nameEn: 'Automobile training course',
          nameVn: 'Bồi dưỡng kỹ thuật sửa chữa ô tô',
        },
        {
          slugVn: '/danh-muc/thiet-bi-oto-ca-nhan',
          slugEn: '/categories/equipment-private car',
          nameEn: 'Tools for measuring & checking',
          nameVn: 'Dụng cụ đo và kiểm tra',
        },
        {
          slugVn: '/danh-muc/thiet-bi-oto-ca-nhan',
          slugEn: '/categories/equipment-private car',
          nameEn: 'Technical documents',
          nameVn: 'Tài liệu kỹ thuật ô tô',
        },
        {
          slugVn: '/danh-muc/thiet-bi-oto-ca-nhan',
          slugEn: '/categories/equipment-private car',
          nameEn: 'Transportation management',
          nameVn: 'Quản lý vận tải ô tô',
        },
        {
          slugVn: '/danh-muc/thiet-bi-oto-ca-nhan',
          slugEn: '/categories/equipment-private car',
          nameEn: 'Equipment for care',
          nameVn: 'Thiết bị bảo dưỡng & chăm sóc ô tô',
        },
        {
          slugVn: '/danh-muc/thiet-bi-oto-ca-nhan',
          slugEn: '/categories/equipment-private car',
          nameEn: 'Accessory',
          nameVn: 'Thiết bị phụ trợ ô tô',
        },
      ],
    },
    {
      slugVn: '/danh-muc/thiet-bi-moto-xemy',
      slugEn: '/categories/equipment-motorcycle',
      nameEn: 'Equipment for motorcycle',
      nameVn: 'Thiết bị mô tô và xe máy',
      child: [
        {
          slugVn: '/danh-muc/thiet-bi-',
          slugEn: '/categories/equipment-for-motobike-repair',
          nameEn: 'Equipment for motorcycle repair',
          nameVn: 'Thiết bị sửa chữa mô tô, xe máy',
        },
        {
          slugVn: '/danh-muc/thiet-bi-oto-ca-nhan',
          slugEn: '/categories/equipment-private car',
          nameEn: 'Equipment for motorcycle training',
          nameVn: 'Thiết bị đào tạo nghề mô tô, xe máy',
        },
        {
          slugVn: '/danh-muc/thiet-bi-oto-ca-nhan',
          slugEn: '/categories/equipment-private car',
          nameEn: 'Motorcycle training course',
          nameVn: 'Bồi dưỡng kỹ thuật sửa chữa mô tô, xe máy',
        },
        {
          slugVn: '/danh-muc/thiet-bi-oto-ca-nhan',
          slugEn: '/categories/equipment-private car',
          nameEn: 'Tools for measuring & checking',
          nameVn: 'Dụng cụ đo & kiểm tra',
        },
        {
          slugVn: '/danh-muc/thiet-bi-oto-ca-nhan',
          slugEn: '/categories/equipment-private car',
          nameEn: 'Technical documents',
          nameVn: 'Tài liệu kỹ thuật mô tô, xe máy',
        },
        {
          slugVn: '/danh-muc/thiet-bi-oto-ca-nhan',
          slugEn: '/categories/equipment-private car',
          nameEn: 'Equipment for care',
          nameVn: 'Thiết bị bảo dưỡng & chăm sóc mô tô, xe máy',
        },
      ],
    },
    {
      slugVn: '/danh-muc/thiet-bi-cong-nghiep',
      slugEn: '/categories/equipment-industry',
      nameEn: 'Equipment for industry',
      nameVn: 'Thiết bị công nghiệp',
      child: [
        {
          slugVn: '/danh-muc/thiet-bi-',
          slugEn: '/categories/equipment-for-motobike-repair',
          nameEn: 'Equipment for measurement & testing',
          nameVn: 'Thiết bị đo lường & kiểm tra',
        },
        {
          slugVn: '/danh-muc/thiet-bi-oto-ca-nhan',
          slugEn: '/categories/equipment-private car',
          nameEn: 'Equipment for protection & security',
          nameVn: 'Thiết bị bảo vệ & an toàn',
        },

        {
          slugVn: '/danh-muc/thiet-bi-oto-ca-nhan',
          slugEn: '/categories/equipment-private car',
          nameEn: 'Equipment for communication & industrial control',
          nameVn: 'Thiết bị truyền thông & điều khiển công nghiệp',
        },
      ],
    },
    {
      slugVn: '/danh-muc/thiet-bi-y-te-moi-truong',
      slugEn: '/categories/equipment-health-environment',
      nameEn: 'Equipment for health & environment',
      nameVn: 'Thiết bị y tế & môi trường',
      child: [
        {
          slugVn: '/danh-muc/thiet-bi-',
          slugEn: '/categories/equipment-for-motobike-repair',
          nameEn: 'Equipment for health',
          nameVn: 'Thiết bị y tế',
        },
        {
          slugVn: '/danh-muc/thiet-bi-oto-ca-nhan',
          slugEn: '/categories/equipment-private car',
          nameEn: 'Equipment for enviroment',
          nameVn: 'Thiết bị môi trường',
        },
      ],
    },
    {
      slugVn: '/danh-muc/phu-kien-thiet-bi',
      slugEn: '/categories/equipment-accessories',
      nameEn: 'Equipment accessories',
      nameVn: 'Phụ kiện thiết bị',
    },
    {
      slugVn: '/danh-muc/san-pham-khuyen-mai-hom-nay',
      slugEn: '/categories/hot-promotion-today',
      nameEn: 'Hot promotion today!',
      nameVn: 'Sản phẩm khuyến mại hôm nay',
    },
  ];

  const locations = [
    { id: 13, name: 'Viet nam' },
    { id: 4, name: 'US' },
    { id: 9, name: 'UK' },
  ];

  const posts = [
    {
      title: 'Ý nghĩa và phong thủy 3 số cuối sim của bạn',
      createdAt: '30/07/2019',
      author: 'Dan Vu',
      feature:
        'https://nhadathaiphong.city/uploads/images/1546463285gnxdw-12-con-giap-636812483709798421.jpg',
      brief:
        'Sở hữu một sim phong thủy đẹp sẽ đem lại cho bạn nhiều may mắn, tài lộc, hạnh phúc. Sở hữu một sim phong thủy đẹp sẽ đem lại cho bạn nhiều may mắn, tài lộc, hạnh phúc., Vì vậy hãy cùng tìm hiểu từng cách để xem cách nào phù hợp và áp dụng nhé.Sở hữu một sim phong thủy đẹp sẽ đem lại cho bạn nhiều may mắn, tài lộc, hạnh phúc. Vì vậy hãy cùng tìm hiểu từng cách để xem cách nào phù hợp và áp dụng nhé.',
    },
    {
      title: 'Ý nghĩa và phong thủy 3 số cuối sim của bạn',
      createdAt: '30/07/2019',
      author: 'Dan Vu',
      feature:
        'https://nhadathaiphong.city/uploads/images/1546463285gnxdw-12-con-giap-636812483709798421.jpg',
      brief:
        'Sở hữu một sim phong thủy đẹp sẽ đem lại cho bạn nhiều may mắn, tài lộc, hạnh phúc. Vì vậy hãy cùng tìm hiểu từng cách để xem cách nào phù hợp và áp dụng nhé.',
    },
  ];

  const post = {
    title: 'Ý nghĩa và phong thủy 3 số cuối sim của bạn',
    createdAt: '30/07/2019',
    author: 'Dan Vu',
    feature:
      'https://nhadathaiphong.city/uploads/images/1546463285gnxdw-12-con-giap-636812483709798421.jpg',
    content: `Bạn luôn tò mò, bạn luôn muốn tìm hiểu ý nghĩa 3 số cuối sim điện thoại của mình, đừng bỏ qua bài viết dưới đây của chúng tôi. Đây chắc chắn sẽ là những thông tin vô cùng hữu ích dành cho tất cả mọi người do SimSoDep.Com bật mí cho các bạn.

  ◉ Ý nghĩa 3 số cuối điện thoại:
  
  - Theo phong thủy, mỗi con số đều mang những ý nghĩa riêng biệt và liên quan mật thiết đến con người. Đặc biệt chính là số Sim điện thoại mà bạn đang sở hữu. Chúng ta đều hiểu rằng chính số điện thoại mà mình đang sử dụng cũng ảnh hưởng trực tiếp đến số mệnh của mình.
  
  - Nhưng trên thực tế chúng ta cần phải dựa vào rất nhiều những yếu tố khác nhau để biết được ý nghĩa số điện thoại mình đang sử dụng có phù hợp với số mệnh, và mang lại tiền tài và may mắn cho mình hay không. Một trong các yếu tố quan trọng nhất mà chúng ta cần phải xem xét, đó là ý nghĩa 3 số cuối điện thoại.
  
  ◉ Ý nghĩa cụ thể của 3 số cuối Sim điện thoại của bạn:
  
  - Với bộ 3 số cuối là 111: Số 1 đứng riêng lẻ là một con số tượng trưng cho vị trí cao nhất, vị trí “độc tôn” đại diện cho sự độc đáo, riêng biệt. Bộ 3 số 1 kết hợp, bổ trợ cho nhau tạo nên một sức mạnh phi thường để vượt qua mọi rào cản, khó khan và thách thức trong cuộc sống.
  
  - Bộ 3 số cuối là 222: Đây là bộ số tượng trưng cho sự cân bằng, hoàn hảo, là một sự khỏi đầu đầy tốt lành, và may mắn. Khách hàng, đặc biệt là những người làm trong lĩnh vực kinh doanh, buôn bán thường rất ưa chuộng số Sim với bộ số này.
  
  - Bộ 3 số cuối 333: Theo phong thủy, đây là bộ số dành riêng cho những người mệnh Hỏa. Số 3 thể hiện tài lộc, giàu sang và sung túc.
  
  - Bộ 3 số cuối 444: Có rất nhiều quan niệm cho rằng, số 4 là con số không may mắn, “tứ” có cách đọc gần giống với “tử”- tức là chết. Tuy nhiên, thực tế thì ý nghĩa của số 4 lại hoàn toàn không phải là như vậy. Con số 4 tượng trưng cho 4 phương: Bắc, Nam, Đông, Tây, tượng trưng cho 4 mùa: xuân, hạ, thu, đông. Số Sim với 3 số cuối 444 là chính là biểu tượng của sự trọn vẹn, toàn diện. Đồng thời cũng mang ý nghĩa vĩnh cửu, mang đến cho chủ sở hữu nhiều may mắn trong cuộc sống và công việc.
  
  - Bộ 3 số 555: Số Sim với 3 số cuối 555 được khách hàng đánh giá là một số Sim có ý nghĩa đẹp nhất và là lựa chọn của rất nhiều người. Con số 5 tượng trưng cho quyền uy, sức mạnh, là con số của những người quyền quý, của sự thăng tiến.
  
  - Bộ 3 số cuối 666: Những người mệnh Kim và mệnh Thủy sẽ không thể bỏ qua số điện thoại có bộ số này. Số 6 đồng âm với “lộc”- thịnh vượng, tiền tài và may mắn. Ngoài ra, số 6 còn là con số với ý nghĩa bình an và thịnh vượng.
  
  - Bộ 3 số cuối 777: Bộ số hội tụ được tinh hoa của đất trời tượng trưng cho quyền năng của hệ mặt trời, thể hiện sự vẹn toàn, sức mạnh vĩ đại để vượt qua nhiều khó khăn và thử thách trong cuộc sống.
  
  - Bộ 3 số cuối 888: Ba số 8 đứng liền nhau, tạo thành bộ tam hoa thể hiện sự thăng hoa, viên mãn và vẹn toàn.
  
  - Bộ 3 số cuối 999: Số 9 con số tượng trưng cho sự trường tồn bất diệt. Là con số lớn nhất trong dãy số tự nhiên từ 1-9, số 9 thể hiện sự viên mãn tròn đầy, và sự quyền lực vĩnh cửu.
  
  ◉ Số cuối điện thoại may mắn: Cụ thể chủ sở hữu cần phải biết được yếu tố ngũ hành liên quan đến bản thân mình để chắc chắn đuôi số điện thoại hợp với tuổi của mình.
  
  - Người tuổi Tý và tuổi Hợi
  
  Người tuổi Tý và người tuổi Hợi là những người thuộc mệnh Thủy. Con số hợp với mệnh này sẽ là 1, 2, 7 và 8
  
  Trong ngũ hành phong thủy, số 1 và số 2 là mệnh Mộc, với quan niệm Thủy sinh Mộc, nên sự xuất hiện của 2 con số này trong số Sim sẽ mang đến cho chủ nhân thêm nhiều tài lộc, may mắn và sức khỏe
  
  Bên cạnh đó, số 7 và số 8 thuộc hành Kim, quan niệm Kim sinh Thủy, nhân sinh kim vận thập toàn, cực kỳ tốt, hỗ trợ và thúc đẩy tài lộc và thịnh vượng
  
  - Người tuổi Dần và tuổi Mão
  
  4 con số may mắn với những người mệnh Mộc này đó là: 0, 3, 4 và 9
  
  Số 3 và 4 là 2 con số thuộc hành Hỏa, quan niệm là Mộc sinh Hỏa. Số 0 và 9 thuộc hành Thủy, quan niệm xưa là Thủy sinh Mộc. Chính vì vậy, người mệnh Mộc dùng số điện thoại có các số trên ở đuôi thì phúc vận sẽ càng thêm vượng, tài lộc dồi dào.
  
  - Người tuổi Tỵ và tuổi Ngọ
  
  Đây là những người mệnh Hỏa, hợp với 4 con số 1, 2, 5 và 6
  
  Theo phong thủy, Mộc (số 1,2) sinh Hỏa, Hỏa sinh Thổ (số 5, 6) sẽ giúp chủ nhận quan vận hanh thông, tài lộc đầy nhà, đạt được thành công lớn
  
  - Người tuổi Thân và Dậu
  
  Con số may mắn: 0, 5, 6 và 9
  
  Tuổi Thân và tuổi Dậu là những người thuộc mệnh Kim, quan niệm xưa là Thổ sinh Kim, Kim lại sinh Thủy, thuận về tài chính và công danh. Các con số này sẽ giúp người tuổi Thân và tuổi Dậu có quý nhân phù trợ, dễ dàng phát tài phát lộc
  
  - Người tuổi Sửu, tuổi Mùi, tuổi Thìn và tuổi Tuất
  
  Đây là những người thuộc ngũ hành mệnh Thổ. 4 con số may mắn dành cho những người tuổi này là 3,4, 7 và 8
  
  Số 3 và số 4 là 2 con số thuộc hành Hỏa, Hỏa sinh Thổ, vận khí sẽ chuyển biến tốt đẹp, sức khỏe an khang và hỷ sự đầy nhà
  
  Số 7 và số 8 thuộc hành Kim, quan niệm Thổ sinh Kim, nên danh lợi sẽ song toàn
  
  Chính vì thế, chủ sở hữu nên chọn cho bản thân những số Sim thuộc các con số đuôi may mắn, thuộc hành tương sinh với bản mệnh của mình. Đặc biệt lưu ý, tuyệt đối không lựa chọn số thuộc hành khắc với bản mệnh. Đây là một điều cực kỳ tối kỵ mà chúng ta nên tránh.
  
  ◉ Ý nghĩa số điện thoại theo phong thủy
  
  - Để biết được số Sim điện thoại mà mình đang sở hữu có phù hợp về phong thủy và đem lại tài lộc cho mình hay không, cần phải xem xét vào rất nhiều những yếu tố khác nhau. Ngoài ý nghĩa các số trong đuôi điện thoại, ngũ hành trong Sim, cần xem xét đến thuyết âm dương trong số Sim.
  
  - Trong thuyết âm dương, các số chẵn là các số âm, và các số lẻ là số dương. Số Sim đẹp là số Sim điện thoại có được tỉ lệ cân bằng âm dương, lượng các số chẵn bằng lượng các số lẻ. Điều này sẽ giúp cho cuộc sống, công việc của chủ nhân luôn luôn cân bằng, hài hòa và hạnh phúc.
  
  - Nếu không có sự cân bằng, âm nhiều hơn dương, hoặc dương nhiều hơn âm thì sẽ mất cân bằng, mất đi nền tảng vững chắc trong cuộc sống, cũng như trong công việc.
  
  Trong bài viết trên chúng tôi cung cấp toàn bộ thông tin chi tiết và cụ thể nhất về ý nghĩa 3 số cuối điện thoại, cũng như những yếu tố mà Quý khách hàng cần phải nắm được để biết chắc chắn số Sim có phù hợp với mình hay không. Mong rằng, các bạn có thể dễ dàng biết được số điện thoại mình đang sở hữu và sử dụng nói lên điều gì. Xin cảm ơn vì đã quan tâm đến bài viết của chúng tôi!`,
    brief:
      'Sở hữu một sim phong thủy đẹp sẽ đem lại cho bạn nhiều may mắn, tài lộc, hạnh phúc. Vì vậy hãy cùng tìm hiểu từng cách để xem cách nào phù hợp và áp dụng nhé. Nếu không có sự cân bằng, âm nhiều hơn dương, hoặc dương nhiều hơn âm thì sẽ mất cân bằng, mất đi nền tảng vững chắc trong cuộc sống, cũng như trong công việc.',
  };

  const pagination = {
    pages: 2,
  };

  const staffs = [
    {
      avatar: '/assets/images/global/images/our_team_9.jpg',
      nameVn: 'DƯƠNG TUẤN DŨNG',
      nameEn: 'MR. DUONG TUAN DUNG',
      positionVn: 'Giám đốc',
      positionEn: 'Director',
      mobileVn: '0913 001 792',
      mobileEn: '(+84) 913 001 792',
      email: 'dtdauto@gmail.com',
    },
    {
      avatar: '/assets/images/global/images/our_team_7.jpg',
      nameVn: 'NGUYỄN THỊ THU LAN',
      nameEn: 'MS. NGUYEN THI THU LAN',
      positionVn: 'Phó Giám Đốc Tài Chính',
      positionEn: 'Deputy Chief Financial Officer',
      mobileVn: '0913 216 555',
      mobileEn: '(+84) 913 216 555',
      email: 'dtdauto@gmail.com',
    },
    {
      avatar: '/assets/images/global/images/our_team_10.jpg',
      nameVn: 'TRẦN VIỆT PHÚ',
      nameEn: 'MR. TRAN VIET PHU',
      positionVn: 'Phó Giám Đốc Kỹ Thuật',
      positionEn: 'Deputy Technical Director',
      mobileVn: '0918 848 455',
      mobileEn: '(+84) 918 848 455',
      email: 'dtdauto@gmail.com',
    },
  ];

  const addresses = [
    {
      nameVn: 'Địa chỉ 1',
      nameEn: 'Address 1',
      addressVn: 'Tòa D1 Số 93 Cầu giấy, Quận Cầu Giấy, Hà Nội, Việt Nam',
      addressEn: 'No. D1, 93 Alley, Cau Giay st., Hanoi, Viet Nam',
      mobile: '+84 913 001 792',
      email: 'dtdauto@gmail.com',
    },
    {
      nameVn: 'Địa chỉ 2',
      nameEn: 'Address 2',
      addressVn:
        'Floor 3, Building A25, Institute of Physics / Institute of Science and Technology of Vietnam, No. 18 Hoang Quoc Viet, Hanoi, Vietnam',
      addressEn:
        'Floor 3, Building A25, Institute of Physics / Institute of Science and Technology of Vietnam, No. 18 Hoang Quoc Viet, Hanoi, Vietnam',
      mobile: '+84 913 001 792',
      email: 'dtdauto@gmail.com',
    },
    {
      nameVn: 'Địa chỉ 3',
      nameEn: 'Address 3',
      addressVn:
        'Room 2702, W2 Tower,  Sunrise City Central, 23 Nguyen Huu Tho, District 7, Ho Chi Minh city, Vietnam',
      addressEn:
        'Room 2702, W2 Tower,  Sunrise City Central, 23 Nguyen Huu Tho, District 7, Ho Chi Minh city, Vietnam',
      mobile: '+84 913 001 792',
      email: 'dtdauto@gmail.com',
    },
    {
      nameVn: 'Địa chỉ 4',
      nameEn: 'Địa chỉ 4',
      addressVn:
        'No. 1, Mac Dinh Chi street, Ben Nghe ward, District 1, Ho Chi Minh city, Vietnam',
      addressEn:
        'No. 1, Mac Dinh Chi street, Ben Nghe ward, District 1, Ho Chi Minh city, Vietnam',
      mobile: '+84 913 001 792',
      email: 'dtdauto@gmail.com',
    },
  ];

  const onlineContacts = [
    {
      nameVn: 'Management Department',
      nameEn: 'Management Department',
      mobileVn: '091 8858455',
      mobileEn: '(+84) 91 8858455',
    },
    {
      nameVn: 'Technical consultant department',
      nameEn: 'Technical consultant department',
      mobileVn: '091 8858455',
      mobileEn: '(+84) 91 8858455',
    },
    {
      nameVn: 'Officer department',
      nameEn: 'Officer department',
      mobileVn: '043 7845426',
      mobileEn: '(+84) 43 7845426',
    },
    {
      nameVn: 'Research & development department',
      nameEn: 'Research & development department',
      mobileVn: '091 3555416',
      mobileEn: '(+84) 91 3555416',
    },
    {
      nameVn: 'Marketing department',
      nameEn: 'Marketing department',
      mobileVn: '0 43 7845426',
      mobileEn: '(+84) 43 7845426',
    },
  ];

  const offlineContacts = [
    {
      nameVn: 'Sms',
      nameEn: 'Sms',
      mobileVn: '091 300 1792',
      mobileEn: '(+84) 91 300 1792',
      icon: '<i class="fas fa-sms"></i>',
    },
    {
      nameVn: 'Phone',
      nameEn: 'Phone',
      mobileVn: '0 913 555 416',
      mobileEn: '(+84) 913 555 416',
      icon: '<i class="fal fa-phone-rotary"></i>',
    },
    {
      nameVn: 'Fax',
      nameEn: 'FAX',
      mobileVn: '0 43 7845426',
      mobileEn: '(+84) 43 7845426',
      icon: '<i class="far fa-fax"></i>',
    },
    {
      nameVn: 'Email',
      nameEn: 'Email',
      mobileVn: 'dtdauto@gmail.com',
      mobileEn: 'dtdauto@gmail.com',
      icon: '<i class="fal fa-envelope"></i>',
    },
  ];

  const __subjects = [
    {
      id: 3,
      code: '\bD',
      timeVn: '45 ngày',
      timeEn: '45 days',
      priority: 1,
      topicId: 1,
      nameVn: 'LỚP ĐIỆN TỬ XE MÁY TIÊU CHUẨN',
      nameEn: 'Course Standard',
      teacherVn: '<p>xe may 1</p>',
      teacherEn: '<p>3123</p>',
      studentTargetEn: '<p>21312321321</p>',
      studentTargetVn: '<p>xe may 1</p>',
      toolsVn: '<p>xe may 1</p>',
      toolsEn: '<p>321</p>',
      contentVn: '<p>xe may 1</p>',
      contentEn: '<p>321</p>',
      fileVn: '<p>xe may 1</p>',
      fileEn: '<p>3213</p>',
      policyVn: '<p>xe may 1</p>',
      policyEn: '<p>321</p>',
      priceVn: '<p>xe may 1</p>',
      priceEn: '<p>3213</p>',
      informationVn: '<p>3123213213</p>',
      informationEn: '<p>3213213</p>',
      ortherVn: '<p>xe may 1xe may 1xe may 1xe may 1xe may 1<br></p>',
      ortherEn: '<p>3213xe may 1xe may 1xe may 1</p>',
      imageVn: '<p>xe may 1</p>',
      imageEn: '<p>3213</p>',
      isDeleted: 0,
      enable: 1,
      createdDate: '2020-04-17 15:42:00',
      createdBy: 'DaoLQ3',
      modifiedBy: 'DaoLQ3',
      modifiedDate: '2020-05-05 10:58:41',
      course: [
        {
          id: 3,
          priority: 2,
          code: ' Khoa 02',
          subjectId: 3,
          studentTargetEn: ' Khoa 02',
          studentTargetVn: ' Khoa 02',
          nameVn: ' Khoa 02',
          nameEn: ' Khoa 02',
          timeVn: ' Khoa 02',
          timeEn: ' Khoa 02',
          startDateVn: ' Khoa 02',
          startDateEn: ' Khoa 02',
          addressVn: ' Khoa 02',
          addressEn: ' Khoa 02',
          noteVn: ' Khoa 02',
          noteEn: ' Khoa 02',
          enable: 1,
          numberOfStudent: 23,
          isDeleted: 0,
          createdDate: '2020-04-17 14:21:36',
          createdBy: 'DaoLQ3',
          modifiedBy: 'DaoLQ3',
          modifiedDate: '2020-04-17 20:34:52',
        },
        {
          id: 4,
          priority: 213,
          code: '24',
          subjectId: 3,
          studentTargetEn:
            'ELECTRONIC, ELECTRONIC TRAINNING COURSE FOR NEW GENERATION MOTORBIKES/SCOOTERS',
          studentTargetVn: ' Khóa 24',
          nameVn:
            'ELECTRONIC, ELECTRONIC TRAINNING COURSE FOR NEW GENERATION MOTORBIKES/SCOOTERS',
          nameEn:
            'ELECTRONIC, ELECTRONIC TRAINNING COURSE FOR NEW GENERATION MOTORBIKES/SCOOTERS',
          timeVn: '40 ngày',
          timeEn: '40 day',
          startDateVn: '23/06/2020',
          startDateEn: '23/06/2020',
          addressVn: ' Khóa 24',
          addressEn:
            'ELECTRONIC, ELECTRONIC TRAINNING COURSE FOR NEW GENERATION MOTORBIKES/SCOOTERS',
          noteVn: ' Khóa 24',
          noteEn:
            'ELECTRONIC, ELECTRONIC TRAINNING COURSE FOR NEW GENERATION MOTORBIKES/SCOOTERS',
          enable: 1,
          numberOfStudent: 23,
          isDeleted: 0,
          createdDate: '2020-04-17 15:42:00',
          createdBy: 'DaoLQ3',
          modifiedBy: 'DaoLQ3',
          modifiedDate: '2020-05-05 11:41:04',
        },
      ],
    },
    {
      id: 2,
      code: 'xe may 2',
      timeVn: 'xe may 2',
      timeEn: 'xe may 2',
      priority: 3213,
      topicId: 1,
      nameVn: 'Xe May 2',
      nameEn: 'xe may 2',
      teacherVn: '<p>213xe may 2xe may 2</p>',
      teacherEn: '<p>xe may 2</p>',
      studentTargetEn: '<p>xe may 2</p>',
      studentTargetVn:
        '<div>1. Thợ sửa chữa mô tô, xe máy thế hệ xe cũ muốn cập nhật kiến thức mới</div>\n                                <div>2. Giáo viên dạy nghề mô tô, xe máy các trường dạy nghề muốn nâng cao nghiệp vụ</div>\n                                <div>3. Sinh viên, học sinh các trường cao đẳng, đại học kỹ thuật điện, điện tử</div>\n                                <div>4. Thợ sửa chữa điện tử, điện lạnh dân dụng muốn chuyển nghề sửa chữa xe máy</div>\n                                <div>5. Nhân viên phòng tư vấn kỹ thuật muốn cập nhật kiến thức mới</div>\n                                <div>6. Quản lý xưởng muốn hiểu biết thêm về công nghệ mới và đầu tư trang thiết bị</div>\n                                <div>7. Các công ty, doanh nghiệp, đơn vị quân đội, trường đào tạo nghề muốn hợp tác đào tạo bồi dưỡng giáo viên, kỹ thuật viên</div>',
      toolsVn: '<p>321xe may 2</p>',
      toolsEn: '<p>3213xe may 2</p>',
      contentVn: '<p>3213</p>',
      contentEn: '<p>3123</p>',
      fileVn: '<p>321</p>',
      fileEn: '<p>213321</p>',
      policyVn: '<p>3213</p>',
      policyEn: '<p>3213</p>',
      priceVn: '<p>xe may 2<br></p>',
      priceEn: '<p>xe may 2<br></p>',
      informationVn: null,
      informationEn: null,
      ortherVn: '<p>321</p>',
      ortherEn: '<p>12321</p>',
      imageVn: '<p>3213213xe may 2</p>',
      imageEn: '<p>3213xe may 2</p>',
      isDeleted: 0,
      enable: null,
      createdDate: '2020-04-17 14:21:36',
      createdBy: 'DaoLQ3',
      modifiedBy: 'DaoLQ3',
      modifiedDate: '2020-04-17 17:28:36',
      course: [],
    },
  ];

  const products = [
    {
      id: 77,
      madeByVn: 'DTDAuto',
      madeByEn: 'DTDAuto',
      nameVn: 'PCB UNSOLDER - BỘ GHIM MẠCH ĐIỆN KHÔNG CẦN HÀN DÂY',
      nameEn: 'PCB UNSOLDER - ELECTRONIC CIRCUIT HOLDER WITHOUT WELDING',
      descVn: '',
      descEn: '1234',
      slugVn: 'PCB-VN1',
      slugEn: 'PCB-EN1',
      briefEn:
        'PCB UNSOLDER is the name of the toolkit used to connect multiple electrical points on the types of electronic printed circuit boards in the interim period to operate: measuring, diagnosing, testing, loading software, reading and writing data... without the need to solder on the circuit, without leaving traces of interference, repair, convenient for the repairer.',
      briefVn:
        'PCB UNSOLDER là tên gọi của bộ công cụ sử dụng để kết nối đa điểm điện trên các loại bo mạch điện tử trong thời gian tạm thời để thao tác: đo, chẩn đoán, kiểm tra, nạp phần mềm, đọc ghi dữ liệu... mà không cần phải hàn thiếc trên mạch điện, không để lại dấu vết can thiệp, sửa chữa, tiện lợi cho các thợ sửa chữa.',
      categoryId: 4,
      priority: 7,
      isDeleted: 0,
      published: 'published',
      createdDate: '2020-04-04 14:53:53',
      createdBy: 'DaoLQ3',
      modifiedBy: 'DaoLQ3',
      modifiedDate: '2020-04-04 14:53:53',
      images: [
        {
          id: 9,
          url: 'http://dtdauto.zamio.net/upload/1589949273206-Hinh3.jpg',
          product_image: { productId: 51, imageId: 9 },
        },
        {
          id: 15,
          url: 'http://dtdauto.zamio.net/upload/1590026114093-Hinh1.jpg',
          product_image: { productId: 51, imageId: 15 },
        },
        {
          id: 16,
          url: 'http://dtdauto.zamio.net/upload/1590026123174-Hinh4.jpg',
          product_image: { productId: 51, imageId: 16 },
        },
        {
          id: 17,
          url: 'http://dtdauto.zamio.net/upload/1590026135586-Hinh5.jpg',
          product_image: { productId: 51, imageId: 17 },
        },
        {
          id: 18,
          url: 'http://dtdauto.zamio.net/upload/1590026147618-Hinh8.jpg',
          product_image: { productId: 51, imageId: 18 },
        },
        {
          id: 19,
          url: 'http://dtdauto.zamio.net/upload/1590026155643-Hinh9.jpg',
          product_image: { productId: 51, imageId: 19 },
        },
      ],
      options: [
        {
          id: 5,
          productId: 5,
          name: 'Option',
          nameVn: 'Option',
          nameEn: 'Option',
          values: '["110"]',
          position: 1,
          isDeleted: 0,
          createdDate: '2020-06-02 23:18:01',
          createdBy: null,
          modifiedBy: null,
          modifiedDate: '2020-06-02 23:18:01',
        },
      ],
      variants: [
        {
          id: 434,
          sku: 'EC1006',
          productId: 77,
          name: '1100',
          netWeight: 2.5,
          weight: 2.5,
          priceEn: 82,
          priceVn: 1900000,
          option1: '110',
          option2: '0',
          option3: '0',
          position: 1,
          isDeleted: 0,
          createdDate: '2020-05-20 09:19:20',
          createdBy: null,
          modifiedBy: null,
          modifiedDate: '2020-05-20 09:19:20',
        },
        {
          id: 436,
          sku: 'EC1006',
          productId: 77,
          name: '1100',
          netWeight: 2.5,
          weight: 2.5,
          priceEn: 82,
          priceVn: 1900000,
          option1: '110',
          option2: '0',
          option3: '0',
          position: 1,
          isDeleted: 0,
          createdDate: '2020-05-20 09:19:20',
          createdBy: null,
          modifiedBy: null,
          modifiedDate: '2020-05-20 09:19:20',
        },
        {
          id: 435,
          sku: 'EC1006',
          productId: 77,
          name: '1100',
          netWeight: 2.5,
          weight: 2.5,
          priceEn: 82,
          priceVn: 1900000,
          option1: '110',
          option2: '0',
          option3: '0',
          position: 1,
          isDeleted: 0,
          createdDate: '2020-05-20 09:19:20',
          createdBy: null,
          modifiedBy: null,
          modifiedDate: '2020-05-20 09:19:20',
        },
      ],
    },
    {
      id: 11,
      madeByVn: 'DTDAuto',
      madeByEn: 'DTDAuto',
      nameVn: 'MOTOSCAN - THIẾT BỊ XÁC ĐỊNH LỖI MÔ TÔ, XE MÁY PHUN XĂNG ĐIỆN TỬY',
      nameEn: "MOTOSCAN - SCANNER FOR PGM-FI/FI MOTORCYCLES & SCOOTERS",
      descVn: '',
      descEn: '1234',
      slugVn: 'PCB-VN1',
      slugEn: 'PCB-EN1',
      briefEn:
        'PCB UNSOLDER is the name of the toolkit used to connect multiple electrical points on the types of electronic printed circuit boards in the interim period to operate: measuring, diagnosing, testing, loading software, reading and writing data... without the need to solder on the circuit, without leaving traces of interference, repair, convenient for the repairer.',
      briefVn:
        'PCB UNSOLDER là tên gọi của bộ công cụ sử dụng để kết nối đa điểm điện trên các loại bo mạch điện tử trong thời gian tạm thời để thao tác: đo, chẩn đoán, kiểm tra, nạp phần mềm, đọc ghi dữ liệu... mà không cần phải hàn thiếc trên mạch điện, không để lại dấu vết can thiệp, sửa chữa, tiện lợi cho các thợ sửa chữa.',
      categoryId: 4,
      priority: 7,
      isDeleted: 0,
      published: 'published',
      createdDate: '2020-04-04 14:53:53',
      createdBy: 'DaoLQ3',
      modifiedBy: 'DaoLQ3',
      modifiedDate: '2020-04-04 14:53:53',
      images: [
        {
          id: 9,
          url: 'http://dtdauto.zamio.net/upload/1589949273206-Hinh3.jpg',
          product_image: { productId: 51, imageId: 9 },
        },
        {
          id: 15,
          url: 'http://dtdauto.zamio.net/upload/1590026114093-Hinh1.jpg',
          product_image: { productId: 51, imageId: 15 },
        },
        {
          id: 16,
          url: 'http://dtdauto.zamio.net/upload/1590026123174-Hinh4.jpg',
          product_image: { productId: 51, imageId: 16 },
        },
        {
          id: 17,
          url: 'http://dtdauto.zamio.net/upload/1590026135586-Hinh5.jpg',
          product_image: { productId: 51, imageId: 17 },
        },
        {
          id: 18,
          url: 'http://dtdauto.zamio.net/upload/1590026147618-Hinh8.jpg',
          product_image: { productId: 51, imageId: 18 },
        },
        {
          id: 19,
          url: 'http://dtdauto.zamio.net/upload/1590026155643-Hinh9.jpg',
          product_image: { productId: 51, imageId: 19 },
        },
      ],
      options: [
        {
          id: 5,
          productId: 5,
          name: 'Option',
          nameVn: 'Option',
          nameEn: 'Option',
          values: '["110"]',
          position: 1,
          isDeleted: 0,
          createdDate: '2020-06-02 23:18:01',
          createdBy: null,
          modifiedBy: null,
          modifiedDate: '2020-06-02 23:18:01',
        },
      ],
      variants: [
        {
          id: 434,
          sku: 'EC1006',
          productId: 77,
          name: '1100',
          netWeight: 2.5,
          weight: 2.5,
          priceEn: 82,
          priceVn: 1900000,
          option1: '110',
          option2: '0',
          option3: '0',
          position: 1,
          isDeleted: 0,
          createdDate: '2020-05-20 09:19:20',
          createdBy: null,
          modifiedBy: null,
          modifiedDate: '2020-05-20 09:19:20',
        },
        {
          id: 117,
          sku: 'EC1006',
          productId: 77,
          name: '1100',
          netWeight: 2.5,
          weight: 2.5,
          priceEn: 82,
          priceVn: 1900000,
          option1: '110',
          option2: '0',
          option3: '0',
          position: 1,
          isDeleted: 0,
          createdDate: '2020-05-20 09:19:20',
          createdBy: null,
          modifiedBy: null,
          modifiedDate: '2020-05-20 09:19:20',
        },
        {
          id: 118,
          sku: 'EC1006',
          productId: 77,
          name: '1100',
          netWeight: 2.5,
          weight: 2.5,
          priceEn: 82,
          priceVn: 1900000,
          option1: '110',
          option2: '0',
          option3: '0',
          position: 1,
          isDeleted: 0,
          createdDate: '2020-05-20 09:19:20',
          createdBy: null,
          modifiedBy: null,
          modifiedDate: '2020-05-20 09:19:20',
        },
      ],
    },
  ];

  const menuMain = [
    {
      id: '6482ad50-c45f-44b1-a82a-f894253ffa02',
      data: {
        nameEn: 'Product',
        nameVn: 'Product',
        linkEn: '/product',
        linkVn: '/product',
        className: '',
      },
      $$id: '6482ad50-c45f-44b1-a82a-f894253ffa02',
      $$expanded: true,
    },
    {
      id: '9647ecf5-114e-486d-a99e-c088c02aa6a2',
      $$expanded: true,
      data: {
        nameEn: 'Categories',
        nameVn: 'Categories',
        linkEn: 'Categories',
        linkVn: 'Categories',
        className: '',
      },
      $$id: '9647ecf5-114e-486d-a99e-c088c02aa6a2',
    },
  ].map((m1) => {
    const data1 = m1.data;
    const children1 = m1.children || [];
    const mNew1 = {
      vi: { name: data1.nameVn, link: data1.linkVn },
      en: { name: data1.nameEn, link: data1.linkEn },
    };
    const childrenNew =
      children1.length > 0
        ? children1.map((m2) => {
            const data2 = m2.data;
            const mNew2 = {
              vi: { name: data2.nameVn, link: data2.linkVn },
              en: { name: data2.nameEn, link: data2.linkEn },
            };
            return mNew2;
          })
        : null;

    return { ...mNew1, ...{ children: childrenNew } };
  });

  gulp.task('styles', function styles() {
    return gulp
      .src('./src/assets/styles/main.scss')
      .pipe(sass().on('error', sass.logError))
      .pipe(cleanCSS())
      .pipe(gulp.dest('./dist/src/assets/styles'))
      .pipe(
        sync.reload({
          stream: true,
        })
      );
  });

  gulp.task('scripts', () => {
    return gulp
      .src('./src/assets/scripts/app.js')
      .pipe(
        babel({
          presets: ['env'],
        })
      )
      .pipe(uglify())

      .pipe(gulp.dest('./dist/src/assets/scripts'))
      .pipe(
        sync.reload({
          stream: true,
        })
      );
  });

  const languages = [
    {
      image: 'gb',
      code: 'en',
      name: 'English',
      nativeName: 'English',
      href: '/',
    },
    {
      image: 'vn',
      code: 'vi',
      name: 'Vietnamese',
      nativeName: 'Tiếng Việt',
      href: '/vi',
    },
    { image: 'fr', code: 'fr', name: 'French', nativeName: 'français' },
    {
      image: 'es',
      code: 'es',
      name: 'Spanish; Castilian',
      nativeName: 'español, castellano',
    },
    {
      image: 'nl',
      code: 'nl',
      name: 'Dutch',
      nativeName: 'Nederlands, Vlaams',
    },
    { image: 'cn', code: 'zh', name: 'Chiness', nativeName: 'Chiness' },
    {
      image: 'jp',
      code: 'ja',
      name: 'Japanese',
      nativeName: '日本語 (にほんご／にっぽんご)',
    },
    { image: 'pt', code: 'pt', name: 'Portuguese', nativeName: 'Português' },

    { image: 'it', code: 'it', name: 'Italian', nativeName: 'Italiano' },

    { image: 'za', code: 'af', name: 'Afrikaans', nativeName: 'Afrikaans' },
    { image: 'al', code: 'sq', name: 'Albanian', nativeName: 'Shqip' },
    { image: 'et', code: 'am', name: 'Amharic', nativeName: 'አማርኛ' },
    { image: 'ae', code: 'ar', name: 'Arabic', nativeName: 'العربية' },
    {
      image: 'az',
      code: 'az',
      name: 'Azerbaijani',
      nativeName: 'azərbaycan dili',
    },
    { image: 'bd', code: 'bn', name: 'Bengali', nativeName: 'বাংলা' },
    {
      image: 'bg',
      code: 'bg',
      name: 'Bulgarian',
      nativeName: 'български език',
    },
    {
      image: 'ad',
      code: 'ca',
      name: 'Catalan; Valencian',
      nativeName: 'Català',
    },
    { image: 'hr', code: 'hr', name: 'Croatian', nativeName: 'hrvatski' },
    { image: 'cz', code: 'cs', name: 'Czech', nativeName: 'česky, čeština' },
    { image: 'dk', code: 'da', name: 'Danish', nativeName: 'dansk' },

    {
      image: 'ee',
      code: 'et',
      name: 'Estonian',
      nativeName: 'eesti, eesti keel',
    },
    { image: 'de', code: 'de', name: 'German', nativeName: 'Deutsch' },
    { image: 'gr', code: 'el', name: 'Greek, Modern', nativeName: 'Ελληνικά' },
    { image: 'hu', code: 'hu', name: 'Hungarian', nativeName: 'Magyar' },
    { image: 'is', code: 'is', name: 'Icelandic', nativeName: 'Íslenska' },
    {
      image: 'id',
      code: 'id',
      name: 'Indonesian',
      nativeName: 'Bahasa Indonesia',
    },

    { image: 'kh', code: 'km', name: 'Khmer', nativeName: 'ភាសាខ្មែរ' },
    {
      image: 'kp',
      code: 'ko',
      name: 'Korean',
      nativeName: '한국어 (韓國語), 조선말 (朝鮮語)',
    },
    { image: 'la', code: 'lo', name: 'Lao', nativeName: 'ພາສາລາວ' },
    { image: 'lv', code: 'lv', name: 'Latvian', nativeName: 'latviešu valoda' },
    {
      image: 'lt',
      code: 'lt',
      name: 'Lithuanian',
      nativeName: 'lietuvių kalba',
    },
    {
      image: 'mk',
      code: 'mk',
      name: 'Macedonian',
      nativeName: 'македонски јазик',
    },
    {
      image: 'my',
      code: 'ms',
      name: 'Malay',
      nativeName: 'bahasa Melayu, بهاس ملايو‎',
    },
    { image: 'mn', code: 'mn', name: 'Mongolian', nativeName: 'монгол' },
    { image: 'mm', code: 'my', name: 'Burmese', nativeName: 'ဗမာစာ' },
    { image: 'np', code: 'ne', name: 'Nepali', nativeName: 'नेपाली' },
    { image: 'no', code: 'no', name: 'Norwegian', nativeName: 'Norsk' },
    { image: 'ir', code: 'fa', name: 'Persian', nativeName: 'فارسی' },
    { image: 'pl', code: 'pl', name: 'Polish', nativeName: 'polski' },
    {
      image: 'ro',
      code: 'ro',
      name: 'Romanian, Moldavian, Moldovan',
      nativeName: 'română',
    },
    { image: 'ru', code: 'ru', name: 'Russian', nativeName: 'русский язык' },
    { image: 'rs', code: 'sr', name: 'Serbian', nativeName: 'српски језик' },
    {
      image: 'lk',
      code: 'si',
      name: 'Sinhala, Sinhalese',
      nativeName: 'සිංහල',
    },
    { image: 'sk', code: 'sk', name: 'Slovak', nativeName: 'slovenčina' },

    { image: 'ax', code: 'sv', name: 'Swedish', nativeName: 'svenska' },
    { image: 'th', code: 'th', name: 'Thai', nativeName: 'ไทย' },
    { image: 'tr', code: 'tr', name: 'Turkish', nativeName: 'Türkçe' },
    { image: 'ua', code: 'uk', name: 'Ukrainian', nativeName: 'українська' },
    {
      image: 'uz',
      code: 'uz',
      name: 'Uzbek',
      nativeName: 'zbek, Ўзбек, أۇزبېك‎',
    },
  ];

  const menus = [
    {
      vi: { name: 'Tin mới', slug: '/blog' },
      en: { name: 'News', slug: '/news' },
    },
    {
      vi: { name: 'Sản phẩm', slug: '#' },
      en: { name: 'Products', slug: '#' },
      children: [
        {
          vi: { name: 'Thiết bị ô tô', slug: '/danh-muc/link-kien-oto' },
          en: {
            name: 'Equipment for automobile',
            slug: '/categoies/equipment-for-motobike',
          },
          children: [
            {
              vi: { name: 'Thiết bị ô tô', slug: '/danh-muc/link-kien-oto' },
              en: {
                name: 'Equipment for automobile',
                slug: '/categoies/equipment-for-motobike',
              },
            },
            {
              vi: {
                name: 'Thiết bị moto, xe máy',
                slug: '/danh-muc/link-kien-xe-may',
              },
              en: {
                name: 'Equipment for motorcycle',
                slug: '/categoies/equipment-for-auto',
              },
            },
          ],
        },
        {
          vi: {
            name: 'Thiết bị moto, xe máy',
            slug: '/danh-muc/link-kien-xe-may',
          },
          en: {
            name: 'Equipment for motorcycle',
            slug: '/categoies/equipment-for-auto',
          },
          children: [
            {
              vi: { name: 'Thiết bị ô tô', slug: '/danh-muc/link-kien-oto' },
              en: {
                name: 'Equipment for automobile',
                slug: '/categoies/equipment-for-motobike',
              },
            },
            {
              vi: {
                name: 'Thiết bị moto, xe máy',
                slug: '/danh-muc/link-kien-xe-may',
              },
              en: {
                name: 'Equipment for motorcycle',
                slug: '/categoies/equipment-for-auto',
              },
            },
          ],
        },
        {
          vi: {
            name: 'Thiết bị công nghiệp',
            slug: '/danh-muc/link-kien-xe-may',
          },
          en: {
            name: 'Equipment for Industry',
            slug: '/categoies/equipment-for-auto',
          },
        },
        {
          vi: {
            name: 'Thiết bị y tế và môi trường',
            slug: '/danh-muc/link-kien-xe-may',
          },
          en: {
            name: 'Equipment for health & environment',
            slug: '/categoies/equipment-for-auto',
          },
        },
        {
          vi: {
            name: 'Phụ kiện thiết bị',
            slug: '/danh-muc/link-kien-xe-may',
          },
          en: {
            name: 'Equipment accessories',
            slug: '/categoies/equipment-for-auto',
          },
        },
      ],
    },
    {
      vi: { name: 'Đào tạo', slug: '/vi' },
      en: { name: 'Training', slug: '/' },
    },
    {
      vi: { name: 'Giới thiệu', slug: '/gioi-thieu' },
      en: { name: 'About us', slug: '/about-us' },
    },

    {
      vi: { name: 'Liên hệ', slug: '/lien-he' },
      en: { name: 'Contact', slug: '/contact' },
    },
    {
      vi: { name: 'Sản phẩm', slug: '/san-pham' },
      en: { name: 'More' },
      children: [
        {
          vi: { name: 'Dịch vụ Kỹ Thuật', slug: '/danh-muc/link-kien-xe-may' },
          en: {
            name: 'Technical Service',
            slug: '/categoies/equipment-for-auto',
          },
        },
        {
          vi: { name: 'Tài liệu', slug: '/danh-muc/link-kien-xe-may' },
          en: {
            name: 'Data & Document',
            slug: '/categoies/equipment-for-auto',
          },
        },
        {
          vi: {
            name: 'Hỏi & Đáp',
            slug: '/danh-muc/link-kien-xe-may',
          },
          en: {
            name: 'Frequently Asked Questions',
            slug: '/categoies/equipment-for-auto',
          },
        },
      ],
    },
  ];

  const site = {
    logoMainUrl: '/assets/images/logos/logo-dtd.png',
    logoShadowUrl: '/assets/images/logos/logo-shadow.png',
    phone: '+84 912122296',
    title: 'Kế hoạch đào tạo năm 2020 của DTDAuto Việt Nam',
    sologan: 'Moving forward',
    language: 'en',
    languages,
    menus,
    menuMain,
    staffs,
    addresses,
    onlineContacts,
    offlineContacts,
  };

  gulp.task('html', async () => {
    const i18n = await i18next.use(Backend).init({
      backend: { loadPath: 'locales/{{lng}}/{{ns}}.json' },
      ns: ['translation', 'path', 'common'],
      fallbackLng: 'en',
      defaultNS: ['translation'],
      fallbackNS: ['common'],
      lng: 'vi',
      preload: ['vi', 'en'],
    });
    return gulp
      .src('./src/views/pages/**/*.twig')
      .pipe(
        twig({
          data: {
            language: 'en',
            languages,
            t: i18n,
            site,
            pagination,
            categories,
            orderData,
            banners,
            locations,
            products,
            productsRelated: products,
            product: products[0],
            helpers: { formatNumber, getProductByLocale, getProductString },
            baseUrl,
            posts,
            post,
            subjects: JSON.stringify(__subjects),
          },
        })
      )
      .pipe(gulp.dest('./dist/src'))
      .pipe(
        sync.reload({
          stream: true,
        })
      );
  });

  gulp.task('server', (done) => {
    if (sync)
      sync.init({
        server: {
          baseDir: './dist/src',
        },
      });
    done();
  });

  gulp.task('watch', (done) => {
    gulp.watch('./src/assets/scripts/*.js', gulp.series('scripts'));

    gulp.watch(
      ['./src/assets/styles/main.scss', './src/views/**/*.scss'],
      gulp.series(['styles'])
    );
    gulp.watch(['./src/views/**/*.twig'], gulp.series('html'));
    done();
  });

  gulp.task('copy-sw-scripts', function () {
    return gulp
      .src([
        'node_modules/sw-toolbox/sw-toolbox.js',
        'src/assets/scripts/sw/runtime-caching.js',
      ])
      .pipe(gulp.dest('dist/src/assets/scripts/sw'));
  });

  gulp.task(
    'generate-service-worker',
    gulp.series('copy-sw-scripts'),
    function () {
      const rootDir = 'dist/src/assets';
      const filepath = path.join(rootDir, 'service-worker.js');

      return swPrecache.write(filepath, {
        // Used to avoid cache conflicts when serving on localhost.
        cacheId: pkg.name || 'dan-vu',
        // sw-toolbox.js needs to be listed first. It sets up methods used in runtime-caching.js.
        importScripts: [
          'scripts/sw/sw-toolbox.js',
          'scripts/sw/runtime-caching.js',
        ],
        staticFileGlobs: [
          // Add/remove glob patterns to match your directory setup.
          `${rootDir}/images/**/*`,
          `${rootDir}/scripts/**/*.js`,
          `${rootDir}/styles/**/*.css`,
          `${rootDir}/*.{html,json}`,
        ],
        // Translates a static file path to the relative URL that it's served from.
        // This is '/' rather than path.sep because the paths returned from
        // glob always use '/'.
        stripPrefix: rootDir + '/',
      });
    }
  );

  exports.default = gulp.task(
    'default',
    gulp.parallel(
      'styles',
      'scripts',
      'html',
      'watch',
      'generate-service-worker',
      'server'
    )
  );
})();
