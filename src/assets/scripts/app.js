'use strict';

var app = ((currencyjs) => {
  const BASE_API = window.BaseUrl;
  const isVietnam = window.Language === 'vi';
  const symbol = isVietnam ? 'VND' : '$';
  const precision = symbol === 'VND' ? 0 : 2;

  const rateUsd2Vnd = typeof window.rateUsd2Vnd === 'number' ? window.rateUsd2Vnd : 23000;

  /* page product detail begin */
  if (typeof LazyLoad !== 'undefined') {
    new LazyLoad();
  }
  if (typeof Swiper !== 'undefined') {
    new Swiper('.slider-banners', {
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },
      centeredSlides: true,
      autoplay: {
        delay: 2500,
        disableOnInteraction: false,
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
    });

    new Swiper('.product-related', {
      slidesPerView: 4,
      spaceBetween: 30,
      breakpoints: {
        // when window width is >= 320px
        320: {
          slidesPerView: 1,
        },
        // when window width is >= 480px
        768: {
          slidesPerView: 2,
        },
        1200: {
          slidesPerView: 4,
        },
      },
    });
  }

  // change image swatch
  function onClickImageThumb(img) {
    var expandImg = document.getElementById('image-main');
    // Use the same src in the expanded image as the image being clicked on from the grid
    expandImg.src = img.src;
  }
  /* page product detail end */

  /* page shopping cart and heading cart begin */
  const i18n = {
    vi: {
      product: 'sản phẩm',
      notificationLoading: 'Đang tiến hành thành toán',
      notificationSuccessStatus: 'Đặt hàng thành công',
      notificationSuccessMessage: 'Chúng tôi sẽ liên lạc với bạn ngay. Cảm ơn bạn!',
      notificationGotoProduct: '<a href="/danh-muc/tat-ca-san-pham">Trở về trang sản phẩm</a>',
      notificationUnSuccesStatus: 'Đặt hàng không thành công',
      notificationUnSuccesMessage: 'Vui lòng gọi điện cho chúng tôi để được giải đáp',
      errorEmpty: 'Không có sản phẩm nào trong giỏ, bạn hãy đặt hàng',
      studentSlot: 'Còn trống',
    },
    en: {
      product: 'product',
      notificationLoading: 'Awaiting payment',
      notificationSuccessStatus: 'Order successful',
      notificationSuccessMessage: 'We will contact you shortly. Thank you!',
      notificationGotoProduct: '<a href="/categories/all-products">Go to product page</a>',
      notificationUnSuccessStatus: 'Order failed',
      notificationUnSuccessMessage: 'Please contact us for advice',
      errorEmpty: 'You have no items in your shopping cart, please add product',
      studentSlot: '(Empty)',
    },
  };

  const setStorage = function (type, data) {
    store.set(type, data);
  };

  const getStorage = function (type) {
    try {
      return store.get(type);
    } catch (error) { }
  };

  const setCarts = function (data) {
    setStorage('carts', data);
  };

  const getCarts = function () {
    return getStorage('carts') || [];
  };

  function getAmountVndToUsd(amount) {
    const usd = amount / rateUsd2Vnd;
    return Number(usd.toFixed(2));
  }

  function roudNumber(num) {
    return currencyjs(num, { precision: precision, symbol: '' }).value;
  }

  function getCurrency(num) {
    const amount = currencyjs(num, { precision: precision, symbol: '' }).format();
    return `${amount} ${symbol}`;
  }

  function getState() {
    return {
      isVietnam: isVietnam,
      symbol: symbol,
      rateUsd2Vnd: rateUsd2Vnd,
      list: getCarts(),
      products: [],
      subPrice: 0,
      totalPrice: 0,
      shippingFee: 0,
      paymentFee: 0,
      ratePaymentFee: 0.05,

      countries: [],
      cities: [],
      shippingMenthods: [
        { name: 'DHL', active: false, disabled: true },
        { name: 'ARAMEX', active: false, disabled: true },
        { name: 'EMS', active: false, disabled: true },
        { name: 'FEDEX', active: false, disabled: true },
        { name: 'DPEX', active: false, disabled: true },
        { name: 'TNT', active: false, disabled: true },
      ],

      orderModal: {
        isShow: false,
        status: '12',
        message: 'ok',
        loadingMessage: '',
        isLoading: false,
      },
      formOrder: {
        customerName: '',
        email: '',
        phone: '',
        note: '',
        shippingCountry: '',
        shippingCity: '',
        shippingCityName: '',
        shippingAddress: '',
        shippingMethod: '',
        paymentMethod: 'PAYPAL',
      },

      toggleAddToCart(item) {
        const value = { quantity: Number(item.quantity), id: Number(item.id) };
        const itemExist = this.findInCart(value.id);
        if (!itemExist) {
          this.ok = [];
          this.list.push(value);
        } else {
          this.list = this.list.filter((l) => l.id !== value.id);
        }

        // setCarts(this.list);
        this.renderProductButtonCart();
      },
      clearOrderForm() {
        this.formOrder = {
          customerName: '',
          email: '',
          phone: '',
          note: '',
          shippingCountry: '',
          shippingCity: '',
          shippingAddress: '',
          shippingMethod: '',
          paymentMethod: 'PAYPAL',
        };
        this.removeAllToCart();
        this.products = [];
      },
      addToCart(item) {
        if (!item) {
          return;
        }
        if (!item.id) {
          return;
        }
        const value = { quantity: Number(item.quantity), id: Number(item.id) };
        const itemExist = this.findInCart(value.id);
        if (!itemExist) {
          this.list = this.list.concat([value]);
          // setCarts(this.list);
          this.renderProductButtonCart();
        }
      },

      fetchOrder() {
        const productIds = this.list.map((pro) => {
          return { quantity: Number(pro.quantity), variantId: Number(pro.id) };
        });

        const dataFormOrder = {
          currency: this.symbol,
          customerName: this.formOrder.customerName,
          email: this.formOrder.email,
          phone: this.formOrder.phone,
          note: this.formOrder.note,
          shippingCountry: null,
          shippingCity: null,
          shippingAddress: this.formOrder.shippingAddress,
          shippingCityName: this.formOrder.shippingCityName,
          shippingMethod: this.formOrder.shippingMethod,
          paymentMethod: this.formOrder.paymentMethod,

          cartItem: productIds,
          shippingFee: this.shippingFee,
          paymentFee: this.paymentFee,
        };

        if (+this.formOrder.shippingCountry > 0) {
          dataFormOrder.shippingCountry = +this.formOrder.shippingCountry;
          const country = this.countries.find((c) => {
            return c.id === dataFormOrder.shippingCountry;
          });
          dataFormOrder.shippingCountryName = country ? country.name : '';
        }

        if (+this.formOrder.shippingCity > 0) {
          dataFormOrder.shippingCity = +this.formOrder.shippingCity;
          const city = this.cities.find((c) => {
            return c.id === dataFormOrder.shippingCity;
          });
          dataFormOrder.shippingCityName = city ? city.name : '';
        }

        return $.ajax({
          method: 'POST',
          url: BASE_API + '/staging/api/cart/order',
          contentType: 'application/json',
          data: JSON.stringify(dataFormOrder),
        });
      },

      changeQuantityToCart(item) {
        const value = { quantity: Number(item.quantity), id: Number(item.id) };
        this.list = this.list.map((i) => {
          if (i.id === value.id) {
            i.quantity = value.quantity;
          }
          return i;
        });
        this.fetchShippingFee();
      },

      removeToCart(id) {
        this.list = this.list.filter((cart) => cart.id !== id);
        this.fetchShippingFee();
        this.renderProductButtonCart();
      },

      removeAllToCart() {
        this.list = [];
      },

      findInCart(id) {
        return this.list.find((cart) => cart.id === id);
      },

      renderProductButtonCart() {
        const productsEl = document.querySelectorAll('.cta-addtocart');
        Array.from(productsEl).forEach((el) => {
          const elId = Number(el.dataset.id);
          const itemExist = this.findInCart(elId);
          if (itemExist) {
            el.classList.add('added');
          } else {
            el.classList.remove('added');
          }
        });
      },

      removeProductItem(id) {
        this.removeToCart(id);
        this.products = this.products.filter((prod) => prod.id !== id);
      },

      changeProductQuantityItem(productChanged) {
        this.products = this.products.map((prod) => {
          if (productChanged.id === prod.id) {
            const subPrice = roudNumber(productChanged.quantity * productChanged.price);
            const totalWeight = productChanged.quantity * productChanged.weight;

            prod.quantity = productChanged.quantity;
            prod.totalWeight = totalWeight;
            prod.subPrice = subPrice;
          }
          return prod;
        });

        this.changeQuantityToCart(productChanged);
      },

      fetchShippingFee() {
        let locationId = null;

        if (this.cities.length) {
          locationId = this.formOrder.shippingCity;
        } else {
          locationId = this.formOrder.shippingCountry;
        }

        if (locationId) {
          this.fetchShippingFeeByLocation(locationId);
        }
      },

      changeCoutry(countryId) {
        this.formOrder.shippingCity = null;
        this.fetchCitesByCountry(countryId).then(() => {
          if (!this.cities.length) {
            this.fetchShippingMenthodsByLocation(countryId).then((data) => {
              this.fetchShippingFeeByLocation(countryId);
            });
          }
        });
      },

      changeCity(cityId) {
        this.fetchShippingMenthodsByLocation(cityId).then((data) => {
          this.fetchShippingFeeByLocation(cityId);
        });
      },

      getTotalPriceForPaypal() {
        if (this.symbol === 'VND') {
          return getAmountVndToUsd(this.totalPrice);
        }
        return this.totalPrice;
      },

      validateProd(variant) {
        const { product } = variant;
        const productLocale = this.isVietnam
          ? {
            id: variant.id,
            productId: product.id,
            sku: variant.sku,
            name: product.nameVn,
            brief: product.briefVn,
            quantity: variant.quantity,
            price: roudNumber(variant.priceVn),
            weight: variant.weight,
            netWeight: variant.netWeight,
            feature: product.images[0] ? product.images[0].url : '',
            subPrice: roudNumber(variant.quantity * variant.priceVn),
            totalWeight: variant.quantity * variant.weight,
          }
          : {
            id: variant.id,
            productId: product.id,
            sku: variant.sku,
            name: product.nameEn,
            brief: product.briefEn,
            quantity: variant.quantity,
            price: roudNumber(variant.priceEn),
            weight: variant.weight,
            netWeight: variant.netWeight,
            feature: product.images[0] ? product.images[0].url : '',
            subPrice: roudNumber(variant.quantity * variant.priceEn),
            totalWeight: variant.quantity * variant.weight,
          };

        return productLocale;
      },

      fetchProductsInCart() {
        this.isLoading = true;
        const url = BASE_API + '/staging/api/product/ids';
        const ids = this.list.map((item) => item.id).filter((id) => !!id);
        $.ajax({
          type: 'POST',
          url: url,
          contentType: 'application/json; charset=utf-8',
          data: JSON.stringify({ ids }),
        }).then((res) => {
          this.isLoading = false;
          const { data, code } = res;
          if (code === 200 && data.rows.length) {
            this.products = data.rows.map((row) => {
              const item = this.list.find((i) => i.id === row.id);
              const product = Object.assign(row, item);
              return this.validateProd(product);
            });
          }
        });
      },

      fetchCitesByCountry(countryId) {
        const url = `${BASE_API}/staging/api/location/${countryId}/city`;
        return $.ajax({
          method: 'GET',
          url: url,
          contentType: 'application/json',
        })
          .then((res) => {
            const { data, code } = res;
            if (code === 200 && data.length) {
              this.cities = data.map((row) => {
                return row;
              });
            } else {
              this.cities = [];
            }
          })
          .fail((e) => {
            console.log(e);
            this.cities = [];
          });
      },

      fetchCountries() {
        const url = BASE_API + '/staging/api/location/country/all';
        return $.ajax({
          method: 'GET',
          url: url,
          contentType: 'application/json',
        })
          .then((res) => {
            const { data, code } = res;
            if (code === 200 && data.length) {
              this.countries = data.map((row) => {
                return row;
              });
            }
          })
          .fail((e) => {
            this.countries = [];
            console.log(e);
          });
      },

      fetchShippingMenthodsByLocation(locationId) {
        const url = BASE_API + '/staging/api/zone/payment-menthods';
        return $.ajax({
          type: 'GET',
          url: url,
          contentType: 'application/json; charset=utf-8',
          data: {
            locationId: Number(locationId),
          },
        })
          .then((res) => {
            const { data, code } = res;
            const shippingMenthods = this.shippingMenthods.map((shippingMenthod) => {
              const found =
                Array.isArray(data) &&
                data.find((d) => {
                  return d.carrier === shippingMenthod.name;
                });

              return Object.assign({}, shippingMenthod, {
                disabled: !found,
              });
            });

            this.shippingMenthods = this.getShippingMethodWithDefault(shippingMenthods);
          })
          .fail((e) => {
            console.log(e);
          });
      },

      getShippingMethodWithDefault(shippingMenthods) {
        let exitShippingDefault = null;
        const ok = shippingMenthods
          .map((shippingMenthod) => {
            if (!shippingMenthod.disabled && this.formOrder.shippingMethod === shippingMenthod.name) {
              exitShippingDefault = shippingMenthod;
              shippingMenthod.active = true;
            }
            return shippingMenthod;
          })
          .map((shippingMenthod) => {
            if (!exitShippingDefault && !shippingMenthod.disabled) {
              shippingMenthod.active = true;
              exitShippingDefault = shippingMenthod;
            } else {
              shippingMenthod.active = false;
            }
            return shippingMenthod;
          });

        if (!exitShippingDefault) {
          this.formOrder.shippingMethod = null;
        } else {
          this.formOrder.shippingMethod = exitShippingDefault.name;
        }
        return ok;
      },

      fetchShippingFeeByLocation(locationId) {
        if (!this.hasShippingMenthodAvailable()) {
          this.shippingFee = 0;
          return;
        }
        const url = BASE_API + '/staging/api/zone/kg';
        return $.ajax({
          type: 'POST',
          url: url,
          contentType: 'application/json; charset=utf-8',
          data: JSON.stringify({
            shippingMethod: this.formOrder.shippingMethod,
            shippingLocationId: Number(locationId),
            items: this.list.map((l) => {
              return {
                quantity: l.quantity,
                variantId: l.id,
              };
            }),
          }),
        })
          .then((res) => {
            const { data, code } = res;
            if (code === 200) {
              const shippingFeeVND = data;
              this.shippingFee = isVietnam ? shippingFeeVND : getAmountVndToUsd(shippingFeeVND);
            }
          })
          .fail((e) => {
            this.shippingFee = 0;
            console.log(e);
          });
      },

      hasShippingMenthodAvailable() {
        const ok = this.shippingMenthods.some((s) => {
          return !s.disabled;
        });
        return ok;
      },

      setTotalPrice() {
        const subTotalPrice = this.shippingFee + this.subPrice;
        const paymentFee = this.ratePaymentFee * subTotalPrice;

        const totalPrice = subTotalPrice + paymentFee;

        this.paymentFee = roudNumber(paymentFee);
        this.totalPrice = roudNumber(totalPrice);
      },

      validatedOrderForm() {
        if (!this.products.length) {
          alert('(Your cart is empty, need add your products) Giỏ hàng bạn không có sản phẩm');
        }

        const $formCustomerInfo = document.getElementById('formCustomerInfo');
        const $formShippingInfo = document.getElementById('formShipping');

        $formCustomerInfo.classList.add('was-validated');
        $formShippingInfo.classList.add('was-validated');

        if ($formCustomerInfo.checkValidity() && $formShippingInfo.checkValidity() && this.hasShippingMenthodAvailable()) {
          return true;
        }
        return false;
      },

      closeOrderModal() {
        this.orderModal = {
          isShow: false,
          isLoading: false,
          status: '',
          message: '',
        };
      },

      updateOrderModal(isSucced) {
        const notifLocate = i18n[window.Language];
        this.orderModal = {
          isShow: true,
          isLoading: false,
          loadingMessage: '',
          status: isSucced ? notifLocate.notificationSuccessStatus : notifLocate.notificationUnSuccessStatus,
          message: isSucced ? notifLocate.notificationSuccessMessage : notifLocate.notificationUnSuccessMessage,
          goto: notifLocate.notificationGotoProduct,
        };
      },

      showOrderModal() {
        const notifLocate = i18n[window.Language];
        this.orderModal = {
          isShow: true,
          isLoading: true,
          loadingMessage: notifLocate.notificationLoading,
        };
      },

      orderCart() {
        const ok = this.validatedOrderForm();
        if (ok) {
          this.showOrderModal();
          this.fetchOrder()
            .then((res) => {
              console.log('orderCart', res);
              this.clearOrderForm();
              setTimeout(() => {
                this.updateOrderModal(true);
              }, 1000);
            })
            .fail((err) => {
              console.log('orderCart', err);
              setTimeout(() => {
                this.updateOrderModal(false);
              }, 1000);
            });
        }
      },

      product: window.productData || {},
      options: [],
      variants: [],
      variantCurrent: {},

      initStateProductDetail() {
        const { options = [], variants = [] } = this.product;
        this.options = options.concat([]);
        this.variants = variants.concat([]);
        if (variants.length > 0) {
          this.variantCurrent = variants[0];
        }
      },
      clickChangeVariant(variantId) {
        variantId = +variantId;

        this.variantCurrent = this.variants.find((v) => v.variantId === variantId);
      },
      initPageShoppingcart() {
        const pageShoppingcart = document.querySelector('.page-shoppingCart');

        if (pageShoppingcart) {
          this.fetchProductsInCart();
          this.fetchCountries();
        }

        this.$watch('list', (list) => {
          setCarts(list);
        })

        this.$watch('products', (products) => {
          const subPrice = products.reduce((current, next) => {
            current += next.subPrice;
            return current;
          }, 0);
          this.subPrice = roudNumber(subPrice);
        });

        this.$watch('formOrder.paymentMethod', (paymentMethod) => {
          this.ratePaymentFee = paymentMethod === 'PAYPAL' ? 0.05 : 0.0;
          this.setTotalPrice();
        });

        this.$watch('formOrder.shippingMethod', (shippingMethod) => {
          this.fetchShippingFee();
        });
        this.$watch('subPrice', () => {
          this.setTotalPrice();
        });
        this.$watch('shippingFee', () => {
          this.setTotalPrice();
        });
        this.$watch('paymentFee', () => {
          this.setTotalPrice();
        });

        if (window.paypal && pageShoppingcart) {
          const that = this;
          window.paypal
            .Buttons({
              style: {
                layout: 'horizontal',
              },
              onClick: (data, actions) => {
                const ok = that.validatedOrderForm();
                if (ok) {
                  return actions.resolve();
                } else {
                  return actions.reject();
                }
              },
              createOrder: (data, actions) => {
                const totalPriceUSD = that.getTotalPriceForPaypal();
                return actions.order.create({
                  purchase_units: [
                    {
                      amount: {
                        currency: 'USD',
                        value: totalPriceUSD,
                      },
                    },
                  ],
                });
              },
              onApprove: (data, actions) => {
                return actions.order.get().then((orderDetails) => {
                  that.showOrderModal();
                  return actions.order.capture().then((details) => {
                    that
                      .fetchOrder()
                      .then((res) => {
                        console.log('orderCart', res);
                        this.clearOrderForm();
                        that.updateOrderModal(true);
                      })
                      .fail((err) => {
                        console.log('orderCart', err);
                        that.updateOrderModal(false);
                      });
                  });
                });
              },
              onError: (data, actions) => { },
            })
            .render('#paypal-button-container');
        }
      },
      initPageProducts() {
        const pageProducts = document.querySelector('.page-products');
        if (pageProducts) {
          this.renderProductButtonCart();
        }
      },
      init() {
        this.initPageProducts();
        this.initPageShoppingcart();
        this.initStateProductDetail();
      },
    };
  }

  /* page shopping cart and heading cart end */

  /* page education start */

  const clazzes = window.__subjects ? validateData(__subjects) : [];

  function getStateEducation() {
    const value = getComboEducationByClazzId();

    return {
      students: [],
      studentsTable: [],
      numberOfStudent: value.contentSubject ? value.contentSubject.numberOfStudent : 0,
      subjects: value.subjects,
      clazzes: value.clazzes,
      contentSubject: value.contentSubject,
      isLoading: false,
      isShowModal: false,
      isApplyLoading: false,
      applyResult: {
        done: false,
        status: '',
        message: '',
      },
      modal: null,

      form: {
        name: '',
        mobile: '',
        email: '',
        address: '',
        numberOfRegister: null,
      },
      isVisiableVideo: false,
      clickClazz(clazzId) {
        this.clazzes = this.clazzes.map((clazz) => {
          const active = clazz.id === clazzId;
          return Object.assign(clazz, { active: active });
        });

        const value = getComboEducationByClazzId(clazzId);
        this.subjects = value.subjects;

        this.contentSubject = value.contentSubject;
        if (this.contentSubject) {
          this.numberOfStudent = this.contentSubject.numberOfStudent;
          this.loadStudents(this.contentSubject.courseId);
        }
      },
      clickCourse(course) {
        const courseId = course.id;
        this.subjects = this.subjects.map((subject) => {
          const active = subject.id === courseId;
          if (active) {
            this.contentSubject.subheading = subject.subheading;
            this.contentSubject.code = subject.code;
          }
          return Object.assign(subject, { active: active });
        });

        const clazz = this.clazzes.find((clazz) => {
          return clazz.id === course.clazzId;
        });

        this.loadStudents(courseId);
        this.contentsDefault = genarateContentCourse(clazz.contents, course);
      },
      fetchApplyCourse(value) {
        const url = BASE_API + '/staging/api/course/addstudent';
        return $.ajax({
          type: 'POST',
          url: url,
          contentType: 'application/json; charset=utf-8',
          data: JSON.stringify(value),
        });
      },
      fetchStudentsByCourse(courseId) {
        const url = `${BASE_API}/staging/api/course/${courseId}/load-students`;
        return $.ajax({
          type: 'GET',
          url: url,
        });
      },
      openCourseModal(subject) {
        this.modal = Object.assign(subject);
        this.isShowModal = true;
      },
      closeCourseModal() {
        this.applyResult = {
          done: false,
          status: '',
          message: '',
        };
        this.isShowModal = false;
        this.isApplyLoading = false;
        this.modal = null;
        this.form = {
          name: '',
          mobile: '',
          email: '',
          address: '',
          numberOfRegister: 1,
        };
      },
      clickApplyRegister(modal) {
        const value = Object.assign(this.form, {
          courseId: modal.id,
          numberOfRegister: 1,
        });
        this.isApplyLoading = true;

        this.fetchApplyCourse(value)
          .done((res) => {
            const { code } = res;
            if (code === 200) {
              setTimeout(() => {
                this.isApplyLoading = false;
                this.applyResult = {
                  done: true,
                  status: 'Quý khách đã đăng ký thành công',
                  message: 'DTDAuto Vietnam sẽ liên hệ với quý khách chậm nhất trong 24h!',
                };
              }, 1500);
            }
          })
          .fail((e) => {
            setTimeout(() => {
              this.isApplyLoading = false;
              this.applyResult = {
                done: true,
                status: 'Quý khách đã đăng ký thất bại',
                message: 'Có lỗi xảy ra, mời quý khách gọi điện đến số của chúng tôi',
              };
            }, 1500);
          });
      },
      loadStudents(courseId) {
        this.fetchStudentsByCourse(courseId)
          .done((res) => {
            const { code } = res;
            if (code === 200) {
              const { data } = res;
              if (data && Array.isArray(data.students)) {
                this.students = data.students.concat([]);
                const still = this.numberOfStudent - this.students.length;
                if (still > 0) {
                  const stillEmpty = Array.from({ length: still }, (v, i) => i).map((a) => {
                    return {
                      name: i18n[window.Language].studentSlot,
                      address: '',
                    };
                  });
                  this.studentsTable = this.students.concat(stillEmpty);
                }
              }
            }
          })
          .fail((e) => {
            this.students = [];
          });
      },
    };
  }

  function getSubjectItems() {
    const subjectItemsLang = {
      en: [
        { title: 'Time:', value: '', code: 'time' },
        { title: 'Code:', value: '', code: 'code' },
      ],
      vi: [
        { title: 'Thời lượng:', value: '', code: 'time' },
        { title: 'Ký hiệu:', value: '', code: 'code' },
      ],
    };
    return subjectItemsLang[window.Language].concat([]);
  }

  function getCourseItems() {
    const courseItemsLang = {
      en: [
        {
          title: 'Time',
          value: '',
          code: 'time',
        },
        {
          title: 'Start Date',
          value: '',
          code: 'startDate',
        },
        {
          title: 'Address',
          value: '',
          code: 'address',
        },
        {
          title: 'Student Target',
          value: '',
          code: 'studentTarget',
        },
        {
          title: 'Note',
          value: '',
          code: 'note',
        },
      ],
      vi: [
        {
          title: 'Thời gian học',
          value: '',
          code: 'time',
        },
        {
          title: 'Ngày khai giảng',
          value: '',
          code: 'startDate',
        },
        {
          title: 'Địa điểm học',
          value: '',
          code: 'address',
        },
        {
          title: 'Đội tượng học viên',
          value: '',
          code: 'studentTarget',
        },
        {
          title: 'Chú ý',
          value: '',
          code: 'note',
        },
      ],
    };
    return courseItemsLang[window.Language].concat([]);
  }

  function getContentItems() {
    const contentItemsLang = {
      en: [
        {
          title: 'A. Who is this training course for?',
          value: '',
          code: 'studentTarget',
        },
        {
          title: 'B. Time',
          value: '',
          code: 'time',
        },
        {
          title: 'C. Location',
          value: '',
          code: 'address',
        },
        {
          title: 'D. Training course information',
          value: '',
          code: 'information',
        },
        {
          title: 'E. Lecturer',
          value: '',
          code: 'teacher',
        },
        {
          title: 'F. Tools',
          value: '',
          code: 'tools',
        },
        {
          title: 'G. Tution Fees',
          value: '',
          code: 'price',
        },
        {
          title: 'H. Training course content',
          value: '',
          code: 'content',
        },
        {
          title: 'I. File',
          value: '',
          code: 'file',
        },
        {
          title: 'J. Right & Policy',
          value: '',
          code: 'policy',
        },
        {
          title: 'K. Related information',
          value: '',
          code: 'orther',
        },
        {
          title: 'L. Course pictures',
          value: '',
          code: 'image',
        },
      ],
      vi: [
        {
          title: 'A. Đối tượng học viên',
          value: '',
          code: 'studentTarget',
        },
        {
          title: 'B. Thời gian học',
          value: '',
          code: 'time',
        },
        {
          title: 'C. Địa điểm học',
          value: '',
          code: 'address',
        },
        {
          title: 'D. Thông tin tuyển sinh',
          value: '',
          code: 'information',
        },
        {
          title: 'E. Giảng viên',
          value: '',
          code: 'teacher',
        },
        {
          title: 'F. Dụng cụ học tập',
          value: '',
          code: 'tools',
        },
        {
          title: 'G. Học phí',
          value: '',
          code: 'price',
        },
        {
          title: 'H. Nội dung, học phần',
          value: '',
          code: 'content',
        },
        {
          title: 'I. Hồ sơ đăng ký khóa học',
          value: '',
          code: 'file',
        },
        {
          title: 'J. Quyền lợi và chính sách',
          value: '',
          code: 'policy',
        },
        {
          title: 'K. Thông tin khác',
          value: '',
          code: 'orther',
        },
        {
          title: 'L. Một số hình ảnh lớp học',
          value: '',
          code: 'image',
        },
      ],
    };
    return contentItemsLang[window.Language].concat([]);
  }

  function getSubjectData(subject) {
    const postfix = window.Language === 'vi' ? 'Vn' : 'En';
    const subjectItems = getSubjectItems(window.Language);

    const items = subjectItems.concat([]).map((item) => {
      switch (item.code) {
        case 'time': {
          item.value = subject[item.code + postfix];
          break;
        }
        case 'code': {
          item.value = subject[item.code + postfix];
          break;
        }
      }
      return Object.assign(item, {});
    });

    const contentItems = getContentItems();

    const contents = contentItems.map((item) => {
      switch (item.code) {
        case 'studentTarget': {
          item.value = subject[item.code + postfix];
          break;
        }
        case 'time': {
          item.value = subject[item.code + postfix];
          break;
        }
        case 'address': {
          item.value = subject[item.code + postfix];
          break;
        }
        case 'information': {
          item.value = `${subject[item.code + postfix]}`;
          break;
        }
        case 'teacher': {
          item.value = subject[item.code + postfix];
          break;
        }
        case 'tools': {
          item.value = subject[item.code + postfix];
          break;
        }
        case 'price': {
          item.value = subject[item.code + postfix];
          break;
        }
        case 'content': {
          item.value = subject[item.code + postfix];
          break;
        }
        case 'file': {
          item.value = subject[item.code + postfix];
          break;
        }
        case 'policy': {
          item.value = subject[item.code + postfix];
          break;
        }
        case 'orther': {
          item.value = subject[item.code + postfix];
          break;
        }
        case 'image': {
          item.value = subject[item.code + postfix];
          break;
        }
      }
      return item;
    });

    const title = subject['name' + postfix];
    const time = subject['time' + postfix];

    const code = subject['code' + postfix];

    const courses = subject.course.map((c) => {
      const courseItems = getCourseItems();
      const _courseItems = courseItems.map((i) => {
        switch (i.code) {
          case 'time': {
            i.value = c[i.code + postfix];
            break;
          }
          case 'startDate': {
            i.value = c[i.code + postfix];
            break;
          }
          case 'address': {
            i.value = c[i.code + postfix];
            break;
          }
          case 'studentTarget': {
            i.value = c[i.code + postfix];
            break;
          }
          case 'note': {
            i.value = c[i.code + postfix];
            break;
          }
        }
        return i;
      });
      const _startDate = c['startDate' + postfix];
      const _time = c['time' + postfix];
      const _code = c['code' + postfix];
      return {
        clazzId: subject.id,
        id: c.id,
        time: _time,
        numberOfStudent: c.numberOfStudent,
        startDate: _startDate,
        title: `${title} ${_code}`,
        items: _courseItems,
        subheading: c['name' + postfix],
        code: _code,
      };
    });
    return {
      id: subject.id,
      items,
      title,
      time,
      code,
      contents,
      courses,
    };
  }

  function genarateContentCourse(contents, course) {
    return contents.map((cont) => {
      switch (cont.code) {
        case 'address': {
          const address = course.items.find((i) => i.code === cont.code);
          cont.value = address.value;
          break;
        }
        case 'time': {
          const address = course.items.find((i) => i.code === cont.code);
          cont.value = address.value;
          break;
        }
      }
      return Object.assign(cont, {});
    });
  }

  function validateData(_subjects) {
    return _subjects.map((sub) => {
      return getSubjectData(sub);
    });
  }

  function getComboEducationByClazzId(clazzId) {
    if (clazzes.length <= 0) {
      return { clazzes: [], subjects: [] };
    }

    if (typeof clazzId !== 'number') {
      clazzId = clazzes[0].id;
    }

    const _clazzes = clazzes.map((c) => {
      const active = c.id === clazzId;
      const courses = c.courses.map((s, index) => {
        const courseActive = index === 0;
        return Object.assign(s, { active: courseActive });
      });
      return Object.assign(c, { active: active, courses });
    });

    const _clazzSelected = _clazzes.find((c) => c.active);

    const _courseSelected = _clazzSelected.courses[0];
    let contentSubject = null;
    if (_courseSelected) {
      const _contents = genarateContentCourse(_clazzSelected.contents, _courseSelected);
      contentSubject = {
        code: _courseSelected.code,
        courseId: _courseSelected.id,
        numberOfStudent: _courseSelected.numberOfStudent,
        contents: _contents,
        title: `${_clazzSelected.title}`,
        subheading: _courseSelected ? _courseSelected.subheading : '',
      };
    }
    return {
      clazzes: _clazzes,
      subjects: _clazzSelected.courses,
      contentSubject: contentSubject,
    };
  }

  return {
    getState,
    getStateEducation,
    getCurrency,
    getStateEducation,
    onClickImageThumb,
  };
  /* page education end */
})(currency);
